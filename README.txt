# Projet Culture Aléatoire : ACCESS

Ce projet vise a vous proposez un résultat de culturer aléatoirement selon le type de culture que vous voulez et selon ce que vous rechercher.
Il inclut des fonctionnalité pouvant récupérer la description, l'image et le titre d'une oeuvre (Film, livre, musique, jeu vidéo) ou d'un lieux (musée).

## Structure du Projet
- **IHM.py**: Le script général pour l'utilisation du projet.
- **GetInfo.py**: Le script pour récupérer des données relatifs au .
- **Ficher "Fonction_MerePoo_Seul"**: Contient chacune des fonctions de GetInfoPoo.py a l'unité .


## Configuration
Système d'exploitation:
    -> Le projet est compatible avec les systèmes d'exploitation Linux et Windows.

Environnement Python:
    -> S'assurer d'avoir Python 3.X installé sur la machine.

Dépendances:
    -> Les dépendances nécessaires au fonctionnement du projet sont répertoriées dans le fichier requirements.txt.
        -> Il est possible de les installer en utilisant la commande : `pip install -r requirements.txt` en plaçant le fichier requirement dans le fichier source.


## Auteurs
- DIXNEUF Arthur
- RACAPÉ Matys
- CARVALHO PORTELA Enzo