
#-------------------------------------------------------------------------------
#import:
from GetInfoPoo import *
from customtkinter import*
from packaging import*
from PIL import Image, ImageTk
import requests
from io import BytesIO
#-------------------------------------------------------------------------------
#mise en page
root = CTk()
root.geometry("720x480")
root.minsize(480, 360)
var=IntVar()
#-------------------------------------------------------------------------------
#frames
frame1=None
frame2=None
frame3=None
frameFin=None
#-------------------------------------------------------------------------------
#Valeur
Return=""
IDF=[]
IDS=[]
IDL=[]
IDJ=[]
ListeFin=[IDF,IDJ,IDS,IDL]
info=Info()
bouton_start=["Cinéma","Musique","Littérature","Musée","Jeu Vidéo"]

#-------------------------------------------------------------------------------
#------------------------------Fonction de fin----------------------------------
#-------------------------------------------------------------------------------
def fin():
    for i in range(len(ListeFin)):
        for j in range(len(ListeFin[i])):
            ListeFin[i].pop()

    for c in root.winfo_children():
        c.destroy()
        
    global frameFin
    frameFin = CTkFrame(root)
    frameFin.pack(expand=YES)

    Titre = CTkLabel(frameFin, width=20, font=("Arial", 15), text=f'{info.nom}')
    Titre.grid(row=0, column=0)

    try:
        Image = CTkLabel(frameFin,image=obtenir_image_tk(info.image),text='')
    except:
        Image = CTkLabel(frameFin,text=info.image)
    Image.grid(row=1, column=0, ipadx=30, ipady=70)



    global CS, return_button_Fin
    CS=CTkButton(root, width=10,text='Recommencer', command=ButtP)
    CS.pack(pady=0, padx=0)
    return_button_Fin=CTkButton(root, width=10,text='Réessayer', command=return_to_previous_frameFin)
    return_button_Fin.pack(pady=0, padx=4)
#-------------------------------------------------------------------------------
#----------------------------Fonctions de cinéma -------------------------------
#-------------------------------------------------------------------------------
def BCsuite():
    """
    BCsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Cinéma' et qui contient pour variable CA, CS, CF, CD, CT
    """
    global frame2
    frame1.destroy()
    frame2 = CTkFrame(root)
    frame2.pack(expand=YES)
    #boutons----------
    CS=CTkButton(frame2, width=25, cursor='hand2',text='Série', command=Ssuite)
    CS.grid(row=1, column=0, pady=10, padx=10, ipadx=35 )

    CF=CTkButton(frame2, width=25, cursor='hand2',text='Film', command=Fsuite)
    CF.grid(row=2, column=0, pady=10, padx=10, ipadx=38 )

    global return_button

    return_button = CTkButton(root, text="Retour", width=10, command=return_to_previous_frame)
    return_button.pack(pady=0, padx=0)

def Ssuite():
    '''
    BASFsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'animé' ou 'Série' ou Film suivant le bouton cinéma
    car elles représente les mêmes sous catégorie
    et contient pour variable BAA, BA, BCo, BCri, BDo, BDra, BF, BE,BMyst, BSFF,
    BGP
    '''
    global frame2, frame3, switch_varS
    return_button.destroy()
    frame2.destroy()
    frame3 = CTkFrame(root)
    frame3.pack(expand=YES)

    switch_varS =[BooleanVar() for _ in range(10)]
    #boutons--------------

    BAA=CTkSwitch(frame3, width=25, cursor='hand2', text='Action & Adventure',command=lambda: switch_eventS(BAA,0),variable=switch_varS[0], onvalue=10759)
    BAA.grid(row=0, column=0, pady=10, padx=10, ipadx=0,)

    BA=CTkSwitch(frame3, width=25, cursor='hand2', text='Animation',command=lambda: switch_eventS(BA,1),variable=switch_varS[1], onvalue=16)
    BA.grid(row=1, column=0, pady=10, padx=10, ipadx=32)

    BCo=CTkSwitch(frame3, width=25, cursor='hand2', text='Comédie',command=lambda: switch_eventS(BCo,2),variable=switch_varS[2], onvalue=35)
    BCo.grid(row=2, column=0, pady=10, padx=10, ipadx=25)

    BCri=CTkSwitch(frame3, width=25, cursor='hand2', text='Crime',command=lambda: switch_eventS(BCri,3),variable=switch_varS[3], onvalue=80)
    BCri.grid(row=3, column=0, pady=10, padx=10, ipadx=28)

    BDo=CTkSwitch(frame3, width=25, cursor='hand2', text='Documentaire',command=lambda: switch_eventS(BDo,3),variable=switch_varS[3], onvalue=99)
    BDo.grid(row=0, column=1, pady=10, padx=10, ipadx=15)

    BDra=CTkSwitch(frame3, width=25, cursor='hand2', text='Drame',command=lambda: switch_eventS(BDra,4),variable=switch_varS[4], onvalue=18)
    BDra.grid(row=1, column=1, pady=10, padx=10, ipadx=23)

    BF=CTkSwitch(frame3, width=25, cursor='hand2', text='Famille',command=lambda: switch_eventS(BF,5),variable=switch_varS[5], onvalue=10751)
    BF.grid(row=2, column=1, pady=10, padx=10, ipadx=20)

    BE=CTkSwitch(frame3, width=25, cursor='hand2', text='Enfant',command=lambda: switch_eventS(BE,6),variable=switch_varS[6], onvalue=10762)
    BE.grid(row=3, column=1, pady=10, padx=10, ipadx=20)

    BMyst=CTkSwitch(frame3, width=25, cursor='hand2', text='Mystère',command=lambda: switch_eventS(BMyst,7),variable=switch_varS[7], onvalue=9648)
    BMyst.grid(row=0, column=2, pady=10, padx=10, ipadx=20)

    BSFF=CTkSwitch(frame3, width=25, cursor='hand2', text='Science-Fiction & Fantaisie',command=lambda: switch_eventS(BSFF,8),variable=switch_varS[8], onvalue=10765)
    BSFF.grid(row=1, column=2, pady=10, padx=10, ipadx=0)

    BGP=CTkSwitch(frame3, width=25, cursor='hand2', text='Guerre & Politique',command=lambda: switch_eventS(BGP,9),variable=switch_varS[9], onvalue=10768)
    BGP.grid(row=2, column=2, pady=10, padx=10, ipadx=20)

    global return_button_C,rechercher_buttonF

    return_button_C=CTkButton(root, width=25, cursor='hand2', text='Retour', command=return_to_previous_frame_C)
    return_button_C.pack(pady=0,padx=0)
    rechercher_buttonF=CTkButton(root, width=25, cursor='hand2', text='Rechercher', command=fins)
    rechercher_buttonF.pack(pady=1,padx=1)

def Fsuite():
    '''
    BASFsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le boutonou Film suivant le bouton cinéma
    et contient pour variable BAct, BAv, BCom, BCr, BDoc, BDr, BFa, BFan, BHist,
    BH, BMus, BRo, BSF, BTf, BTh, BG
    '''
    global frame2, frame3, switch_varF
    return_button.destroy()
    frame2.destroy()
    frame3 = CTkFrame(root)
    frame3.pack(expand=YES)
    switch_varF =[BooleanVar() for _ in range(17)]
    
    #boutons--------------
    BAct=CTkSwitch(frame3, width=25, cursor='hand2', text='Action',command=lambda: switch_eventF(BAct,0),variable=switch_varF[0], onvalue=28)
    BAct.grid(row=0, column=0, pady=10, padx=10, ipadx=0)

    BAv=CTkSwitch(frame3, width=25, cursor='hand2', text='Aventure',command=lambda: switch_eventF(BAv,1),variable=switch_varF[1], onvalue=12)
    BAv.grid(row=1, column=0, pady=10, padx=10, ipadx=32)

    BAn=CTkSwitch(frame3, width=25, cursor='hand2', text='Animation',command=lambda: switch_eventF(BAn,2),variable=switch_varF[2], onvalue=16 )
    BAn.grid(row=2, column=0, pady=10, padx=10, ipadx=25)

    BCom=CTkSwitch(frame3, width=25, cursor='hand2', text='Comédie',command=lambda: switch_eventF(BCom,3),variable=switch_varF[3], onvalue=35 )
    BCom.grid(row=3, column=0, pady=10, padx=10, ipadx=28)

    BCr=CTkSwitch(frame3, width=25, cursor='hand2', text='Crime',command=lambda: switch_eventF(BCr,4),variable=switch_varF[4], onvalue=80 )
    BCr.grid(row=4, column=0, pady=10, padx=10, ipadx=15)

    BDoc=CTkSwitch(frame3, width=25, cursor='hand2', text='Documentaire',command=lambda: switch_eventF(BDoc,5),variable=switch_varF[5], onvalue=99)
    BDoc.grid(row=5, column=0, pady=10, padx=10, ipadx=23)

    BDr=CTkSwitch(frame3, width=25, cursor='hand2', text='Drame',command=lambda: switch_eventF(BDr,6),variable=switch_varF[6], onvalue=18)
    BDr.grid(row=0, column=1, pady=10, padx=10, ipadx=20)

    BFa=CTkSwitch(frame3, width=25, cursor='hand2', text='Famille',command=lambda: switch_eventF(BFa,7),variable=switch_varF[7], onvalue=10751)
    BFa.grid(row=1, column=1, pady=10, padx=10, ipadx=20)

    BFan=CTkSwitch(frame3, width=25, cursor='hand2', text='Fantaisie',command=lambda: switch_eventF(BFan,8),variable=switch_varF[8], onvalue=14)
    BFan.grid(row=2, column=1, pady=10, padx=10, ipadx=20)

    BHist=CTkSwitch(frame3, width=25, cursor='hand2', text='Histoire',command=lambda: switch_eventF(BHist,9),variable=switch_varF[9], onvalue=36)
    BHist.grid(row=3, column=1, pady=10, padx=10, ipadx=0)

    BH=CTkSwitch(frame3, width=25, cursor='hand2', text='Horreur',command=lambda: switch_eventF(BH,10),variable=switch_varF[10], onvalue=27)
    BH.grid(row=4, column=1, pady=10, padx=10, ipadx=20)

    BMus=CTkSwitch(frame3, width=25, cursor='hand2', text='Musique',command=lambda: switch_eventF(BMus,11),variable=switch_varF[11], onvalue=10402)
    BMus.grid(row=5, column=1, pady=10, padx=10, ipadx=20)

    BRo=CTkSwitch(frame3, width=25, cursor='hand2', text='Romance',command=lambda: switch_eventF(BRo,12),variable=switch_varF[12], onvalue=10749)
    BRo.grid(row=0, column=2, pady=10, padx=10, ipadx=20)

    BSF=CTkSwitch(frame3, width=25, cursor='hand2', text='Science-Fiction',command=lambda: switch_eventF(BSF,13),variable=switch_varF[13], onvalue=878)
    BSF.grid(row=1, column=2, pady=10, padx=10, ipadx=20)

    BTf=CTkSwitch(frame3, width=25, cursor='hand2', text='Téléfilm',command=lambda: switch_eventF(BTf,14),variable=switch_varF[14], onvalue=10770)
    BTf.grid(row=2, column=2, pady=10, padx=10, ipadx=20)

    BTh=CTkSwitch(frame3, width=25, cursor='hand2', text='Thriller',command=lambda: switch_eventF(BTh,15),variable=switch_varF[15], onvalue=53)
    BTh.grid(row=3, column=2, pady=10, padx=10, ipadx=20)

    BG=CTkSwitch(frame3, width=25, cursor='hand2', text='Guerre',command=lambda: switch_eventF(BG,16),variable=switch_varF[16], onvalue=10752)
    BG.grid(row=4, column=2, pady=10, padx=10, ipadx=20)

    global return_button_C,rechercher_buttonS

    return_button_C=CTkButton(root, width=25, cursor='hand2', text='Retour', command=return_to_previous_frame_C)
    return_button_C.pack(pady=0,padx=0)
    rechercher_buttonS=CTkButton(root, width=25, cursor='hand2', text='Rechercher', command=finf)
    rechercher_buttonS.pack(pady=1,padx=1)
#-------------------------------------------------------------------------------
#-----------------------------Fonctions de Musique------------------------------
#-------------------------------------------------------------------------------
def BAsuite():
    """
    BAsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Musique'
    et qui contient pour variable AA, AL, AC
    """
    global frame2,textvariable
    frame1.destroy()
    frame2= CTkFrame(root)
    frame2.pack(expand=YES)
    textvariable=StringVar()
    #boutons-------------
    AA=CTkButton(frame2, width=25, cursor='hand2',text='Artiste', command= lambda: typeMusique("artist"))
    AA.grid(row=0, column=0, pady=10,padx=10, ipadx=30)

    AC=CTkButton(frame2, width=25, cursor='hand2',text='Chanson', command= lambda: typeMusique("track"))
    AC.grid(row=0, column=1, pady=10, padx=10, ipadx=25)

    global return_button

    return_button = CTkButton(root, text="Retour", width=10, command=return_to_previous_frame,)
    return_button.pack(pady=0, padx=0)

def MusicSuite():
    '''
    MusicSuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Artiste' ou 'Album' ou 'Chanson' suivant le boutton Musique
    et qui contient pour variable BRap, BRock, BPop, BElec, BV
    '''
    global frame2, frame3,var
    return_button.destroy()
    frame2.destroy()
    frame3 = CTkFrame(root)
    frame3.pack(expand=YES)
    #boutons--------------

    BRap=CTkButton(frame3, width=25, cursor='hand2', text='Rap', command=lambda:finm("rap"))
    BRap.grid(row=0, column=0, pady=10, padx=10, ipadx=32)

    BRock=CTkButton(frame3, width=25, cursor='hand2', text='Rock', command=lambda:finm("rock"))
    BRock.grid(row=1, column=0, pady=10, padx=10, ipadx=30)

    BPop=CTkButton(frame3, width=25, cursor='hand2', text='Pop', command=lambda:finm("pop"))
    BPop.grid(row=1, column=1, pady=10, padx=10, ipadx=25)

    BElec=CTkButton(frame3, width=25, cursor='hand2',text='Electro', command=lambda:finm("electro"))
    BElec.grid(row=0, column=1, pady=10, padx=10, ipadx=28)

    global return_button_M
    return_button_M=CTkButton(root, width=25, cursor='hand2', text='Retour', command=return_to_previous_frame_M)
    return_button_M.pack(pady=0,padx=0)

#-------------------------------------------------------------------------------
#---------------------------Fonctions de Littérature----------------------------
#-------------------------------------------------------------------------------
def BLsuite():
    """
    BLsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Littérature'
    et qui contient pour variable LL, LP, LM, LB, LBI, LE
    """
    global frame2
    frame1.destroy()
    frame2= CTkFrame(root)
    frame2.pack(expand=YES)
    #boutons----------------
    LL=CTkButton(frame2, width=25, cursor='hand2',text='Livre',command=lambda: typeLiterature("book"))
    LL.grid(row=0, column=0, pady=10,padx=10, ipadx=36)

    LP=CTkButton(frame2, width=25 , cursor='hand2', text='Poème',command=lambda: typeLiterature("poem"))
    LP.grid(row=1, column=0, pady=10,padx=10, ipadx=30)

    LM=CTkButton(frame2, width=25, cursor='hand2', text='Manga',command=lambda: typeLiterature("manga"))
    LM.grid(row=2, column=0, pady=10,padx=10, ipadx=31)

    LT=CTkButton(frame2, width=25, cursor='hand2', text='Théatre',command=lambda: typeLiterature("theater"))
    LT.grid(row=3, column=0, pady=10, padx=10, ipadx=25)

    LB=CTkButton(frame2, width=25, cursor='hand2', text='BD',command=lambda: typeLiterature("comics"))
    LB.grid(row=0, column=1, pady=10,padx=10, ipadx=42)

    LBI=CTkButton(frame2, width=25, cursor='hand2', text='Biographie',command=lambda: typeLiterature("biography"))
    LBI.grid(row=1, column=1, pady=10,padx=10, ipadx=20)

    LE=CTkButton(frame2, width=25, cursor='hand2', text='Essai',command=lambda: typeLiterature("essay"))
    LE.grid(row=2, column=1, pady=10, padx=10, ipadx=35)

    

    global return_button
   
    return_button = CTkButton(root, text="Retour", width=10, command=return_to_previous_frame)
    return_button.pack(pady=0, padx=0)

def litsuite():
    global frame2, frame3,switch_varL
    return_button.destroy()
    frame2.destroy()
    frame3 = CTkFrame(root)
    frame3.pack(expand=YES)
    switch_varL =[StringVar() for _ in range(7)]
    if typel =="book":
        BRom=CTkSwitch(frame3, width=25, cursor='hand2', text='Romance',command=lambda: switch_eventL(BRom,0),variable=switch_varL[0], onvalue='romance',offvalue='False')
        BRom.grid(row=0, column=0, pady=10, padx=10, ipadx=22)

        BAct=CTkSwitch(frame3, width=25, cursor='hand2', text='Action',command=lambda: switch_eventL(BAct,1),variable=switch_varL[1], onvalue="action",offvalue='False')
        BAct.grid(row=1, column=0, pady=10, padx=10, ipadx=30)

        BAv=CTkSwitch(frame3, width=25, cursor='hand2', text='Aventure',command=lambda: switch_eventL(BAv,2),variable=switch_varL[2], onvalue='adventure',offvalue='False')
        BAv.grid(row=2, column=0, pady=10, padx=10, ipadx=24)

        BRe=CTkSwitch(frame3, width=25, cursor='hand2', text='Réaliste',command=lambda: switch_eventL(BRe,3),variable=switch_varL[3], onvalue='realistic',offvalue='False')
        BRe.grid(row=0, column=1, pady=10, padx=10, ipadx=25)

        BFi=CTkSwitch(frame3, width=25, cursor='hand2', text='Fiction',command=lambda: switch_eventL(BFi,4),variable=switch_varL[4], onvalue="fiction",offvalue='False')
        BFi.grid(row=1, column=1, pady=10, padx=10, ipadx=30)

        BMy=CTkSwitch(frame3, width=25, cursor='hand2', text='Mystère',command=lambda: switch_eventL(BMy,5),variable=switch_varL[5], onvalue="mystery",offvalue='False')
        BMy.grid(row=2, column=1, pady=10, padx=10, ipadx=25)
        
    if typel=="poem":
        BHa=CTkSwitch(frame3, width=25, cursor='hand2', text='Haiku',command=lambda: switch_eventL(BHa,0),variable=switch_varL[0], onvalue="haiku",offvalue='False')
        BHa.grid(row=0, column=0, pady=10, padx=10, ipadx=34)

        BSon=CTkSwitch(frame3, width=25, cursor='hand2', text='Sonnet',command=lambda: switch_eventL(BSon,1),variable=switch_varL[1], onvalue="sonnet",offvalue='False')
        BSon.grid(row=1, column=0, pady=10, padx=10, ipadx=30)

        BQua=CTkSwitch(frame3, width=25, cursor='hand2', text='Quatrain',command=lambda: switch_eventL(BQua,2),variable=switch_varL[2], onvalue="quatrain",offvalue='False')
        BQua.grid(row=0, column=1, pady=10, padx=10, ipadx=27)

        BRP=CTkSwitch(frame3, width=25, cursor='hand2', text='Recueil de Poésie',command=lambda: switch_eventL(BRP,4),variable=switch_varL[4], onvalue="poetry collection",offvalue='False')
        BRP.grid(row=1, column=1, pady=10, padx=10, ipadx=10)

    if typel=='comics' or typel=='manga':
            BRo=CTkSwitch(frame3, width=25, cursor='hand2', text='Romance',command=lambda: switch_eventL(BRo,0),variable=switch_varL[0], onvalue="love",offvalue='False')
            BRo.grid(row=0, column=0, pady=10, padx=10, ipadx=25)

            BAct=CTkSwitch(frame3, width=25, cursor='hand2', text='Action',command=lambda: switch_eventL(BAct,1),variable=switch_varL[1], onvalue="action",offvalue='False')
            BAct.grid(row=1, column=0, pady=10, padx=10, ipadx=32)

            BAve=CTkSwitch(frame3, width=25, cursor='hand2', text='Aventure',command=lambda: switch_eventL(BAve,2),variable=switch_varL[2], onvalue="adventure",offvalue='False')
            BAve.grid(row=2, column=0, pady=10, padx=10, ipadx=25)

            BMys=CTkSwitch(frame3, width=25, cursor='hand2', text='Mystère',command=lambda: switch_eventL(BMys,3),variable=switch_varL[3], onvalue="mystery",offvalue='False')
            BMys.grid(row=3, column=0, pady=10, padx=10, ipadx=28)

            BPh=CTkSwitch(frame3, width=25, cursor='hand2', text='Philosophie',command=lambda: switch_eventL(BPh,4),variable=switch_varL[4], onvalue="philosophy",offvalue='False')
            BPh.grid(row=0, column=1, pady=10, padx=10, ipadx=15)

            BCo=CTkSwitch(frame3, width=25, cursor='hand2', text='Comédie',command=lambda: switch_eventL(BCo,5),variable=switch_varL[5], onvalue="comedy",offvalue='False')
            BCo.grid(row=1, column=1, pady=10, padx=10, ipadx=23)

            BS=CTkSwitch(frame3, width=25, cursor='hand2', text='Surnaturel',command=lambda: switch_eventL(BS,6),variable=switch_varL[6], onvalue="supernatural",offvalue='False')
            BS.grid(row=2, column=1, pady=10, padx=10, ipadx=20)

    if typel=="biography":
        BAu=CTkSwitch(frame3, width=25, cursor='hand2', text='Autobiographie',command=lambda: switch_eventL(BAu,0),variable=switch_varL[0], onvalue="autobiography",offvalue='False')
        BAu.grid(row=0, column=0, pady=10, padx=10, ipadx=25)

        BEc=CTkSwitch(frame3, width=25, cursor='hand2', text='Écrivain',command=lambda: switch_eventL(BEc,1),variable=switch_varL[1], onvalue="writter",offvalue='False')
        BEc.grid(row=1, column=0, pady=10, padx=10, ipadx=32)

        BAM=CTkSwitch(frame3, width=25, cursor='hand2', text='Artiste Musical',command=lambda: switch_eventL(BAM,2),variable=switch_varL[2], onvalue="musical artist",offvalue='False')
        BAM.grid(row=2, column=0, pady=10, padx=10, ipadx=25)

        BPol=CTkSwitch(frame3, width=25, cursor='hand2', text='Politicien',command=lambda: switch_eventL(BPol,3),variable=switch_varL[3], onvalue="politician",offvalue='False')
        BPol.grid(row=2, column=1, pady=10, padx=10, ipadx=28)

        BPH=CTkSwitch(frame3, width=25, cursor='hand2', text='Personnage Historique',command=lambda: switch_eventL(BPH,4),variable=switch_varL[4], onvalue="historical")
        BPH.grid(row=0, column=1, pady=10, padx=10, ipadx=15)

        BActeur=CTkSwitch(frame3, width=25, cursor='hand2', text='Acteur',command=lambda: switch_eventL(BActeur,5),variable=switch_varL[5], onvalue="actor",offvalue='False')
        BActeur.grid(row=1, column=1, pady=10, padx=10, ipadx=23)
        
    if typel=='essay':
        BSci=CTkSwitch(frame3, width=25, cursor='hand2', text='Scientifique',command=lambda: switch_eventL(BSci,0),variable=switch_varL[0], onvalue="scientific",offvalue='False')
        BSci.grid(row=0, column=0, pady=10, padx=10, ipadx=25)

        BPsy=CTkSwitch(frame3, width=25, cursor='hand2', text='Psychologique',command=lambda: switch_eventL(BPsy,0),variable=switch_varL[1], onvalue="psychological",offvalue='False')
        BPsy.grid(row=1, column=0, pady=10, padx=10, ipadx=32)

        BPoli=CTkSwitch(frame3, width=25, cursor='hand2', text='Politique',command=lambda: switch_eventL(BPoli,0),variable=switch_varL[2], onvalue="political",offvalue='False')
        BPoli.grid(row=2, column=0, pady=10, padx=10, ipadx=25)

        BPhilo=CTkSwitch(frame3, width=25, cursor='hand2', text='Philosophique',command=lambda: switch_eventL(BPhilo,0),variable=switch_varL[3], onvalue="philosphical",offvalue='False')
        BPhilo.grid(row=3, column=0, pady=10, padx=10, ipadx=28)

    if typel=='theater':
        BCo=CTkSwitch(frame3, width=25, cursor='hand2', text='Comédie',command=lambda: switch_eventL(BCo,0),variable=switch_varL[0], onvalue="comedy",offvalue='False')
        BCo.grid(row=0, column=0, pady=10, padx=10, ipadx=34)

        BMC=CTkSwitch(frame3, width=25, cursor='hand2', text='Comédie Musical',command=lambda: switch_eventL(BMC,1),variable=switch_varL[1], onvalue="musical comedy",offvalue='False')
        BMC.grid(row=1, column=0, pady=10, padx=10, ipadx=30)

        BTra=CTkSwitch(frame3, width=25, cursor='hand2', text='Satire',command=lambda: switch_eventL(BTra,2),variable=switch_varL[2], onvalue="satire",offvalue='False')
        BTra.grid(row=2, column=0, pady=10, padx=10, ipadx=27)

        BSa=CTkSwitch(frame3, width=25, cursor='hand2', text='Tragique',command=lambda: switch_eventL(BSa,3),variable=switch_varL[3], onvalue="tragic",offvalue='False')
        BSa.grid(row=0, column=1, pady=10, padx=10, ipadx=0)

        BTraCo=CTkSwitch(frame3, width=25, cursor='hand2', text='Tragicomédie',command=lambda: switch_eventL(BTraCo,4),variable=switch_varL[4], onvalue="tragicomedy",offvalue='False')
        BTraCo.grid(row=1, column=1, pady=10, padx=10, ipadx=10)

    global return_button_L, rechercher_buttonL

    return_button_L=CTkButton(root, width=25, cursor='hand2', text='Retour', command=return_to_previous_frame_L)
    return_button_L.pack(pady=0,padx=0)
    rechercher_buttonL=CTkButton(root, width=25, cursor='hand2', text='Rechercher', command=finl)
    rechercher_buttonL.pack(pady=1,padx=1)


    

#-------------------------------------------------------------------------------
#------------------------------Fonctions de Musée-------------------------------
#-------------------------------------------------------------------------------
def BMsuite():
    """
    BMsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Musée'
    et qui contient pour variable MS, MP, MA
    """
    global frame2
    frame1.destroy()
    frame2= CTkFrame(root)
    frame2.pack(expand=YES)
    #boutons-----------------
    MS=CTkButton(frame2, width=25, cursor='hand2',text='Sculpture',command=lambda:finmu("Sculpture"))
    MS.grid(row=0, column=0, pady=10,padx=10, ipadx=30)

    MP=CTkButton(frame2,width=25, cursor='hand2', text='Peinture',command=lambda:finmu("Peinture"))
    MP.grid(row=1, column=0, pady=10,padx=10, ipadx=30)

    MA=CTkButton(frame2,width=25, cursor='hand2', text='Architecture', command=lambda:finmu("Architecture"))
    MA.grid(row=0, column=1, pady=10,padx=10, ipadx=30)

    global return_button

    return_button = CTkButton(root, text="Retour", width=10, command=return_to_previous_frame)
    return_button.pack(pady=0, padx=0)

#-------------------------------------------------------------------------------
#------------------------------Fonctions de Jeux--------------------------------
#-------------------------------------------------------------------------------
def BJsuite():
    """
    BMsuite() est la fonction permettant d'afficher la suite après
    avoir pressé le bouton 'Jeu Video'
    et qui contient pour variable JR, JF, JA, JP, JS, JSC, JST, JR, JPl, JC,
    JAction, JCa, JMF
    """
    global frame2,switch_varJ
    frame1.destroy()
    frame2= CTkFrame(root)
    frame2.pack(expand=YES)
    switch_varJ =[BooleanVar() for _ in range(12)]
    #boutons-----------------
    JR=CTkSwitch(frame2, width=25, cursor='hand2',text='Role PLaying Game',command=lambda: switch_eventJ(JR,0),variable=switch_varJ[0], onvalue=5 )
    JR.grid(row=0, column=0, pady=10,padx=10, ipadx=5)

    JF=CTkSwitch(frame2, width=25, cursor='hand2',text='First Person Shooter',command=lambda: switch_eventJ(JF,1),variable=switch_varJ[1], onvalue=2 )
    JF.grid(row=1, column=0, pady=10,padx=10, ipadx=3)

    JA=CTkSwitch(frame2, width=25, cursor='hand2',text='Aventure',command=lambda: switch_eventJ(JA,2),variable=switch_varJ[2], onvalue=3 )
    JA.grid(row=2, column=0, pady=10,padx=10, ipadx=39)

    JP=CTkSwitch(frame2, width=25, cursor='hand2',text='Puzzle',command=lambda: switch_eventJ(JP,3),variable=switch_varJ[3], onvalue=7 )
    JP.grid(row=3, column=0, pady=10,padx=10, ipadx=32)

    JS=CTkSwitch(frame2, width=25, cursor='hand2',text='Simulation',command=lambda: switch_eventJ(JS,4),variable=switch_varJ[4], onvalue=14 )
    JS.grid(row=4, column=0, pady=10,padx=10, ipadx=28)

    JSC=CTkSwitch(frame2, width=25, cursor='hand2',text='Sport',command=lambda: switch_eventJ(JSC,5),variable=switch_varJ[5], onvalue=15 )
    JSC.grid(row=5, column=0, pady=10,padx=10, ipadx=22)

    JST=CTkSwitch(frame2, width=25, cursor='hand2',text='Stratégie',command=lambda: switch_eventJ(JST,6),variable=switch_varJ[6], onvalue=10 )
    JST.grid(row=5, column=1, pady=10,padx=10, ipadx=35)

    JPl=CTkSwitch(frame2, width=25, cursor='hand2',text='Plateforme',command=lambda: switch_eventJ(JPl,7),variable=switch_varJ[7], onvalue=83 )
    JPl.grid(row=0, column=1, pady=10,padx=10, ipadx=30)

    JC=CTkSwitch(frame2, width=25, cursor='hand2',text='Combat',command=lambda: switch_eventJ(JC,8),variable=switch_varJ[8], onvalue=6 )
    JC.grid(row=1, column=1, pady=10,padx=10, ipadx=39)

    JAction=CTkSwitch(frame2, width=25, cursor='hand2',text='Action',command=lambda: switch_eventJ(JAction,9),variable=switch_varJ[9], onvalue=4 )
    JAction.grid(row=2, column=1, pady=10,padx=10, ipadx=39)

    JCa=CTkSwitch(frame2, width=25, cursor='hand2',text='Casual',command=lambda: switch_eventJ(JCa,10),variable=switch_varJ[10], onvalue=40 )
    JCa.grid(row=3, column=1, pady=10,padx=10, ipadx=39)

    JMF=CTkSwitch(frame2, width=25, cursor='hand2',text='Multijoueur Massif',command=lambda: switch_eventJ(JMF,11),variable=switch_varJ[11], onvalue=59 )
    JMF.grid(row=4, column=1, pady=10,padx=10, ipadx=39)


    global return_button, rechercher_buttonJ

    return_button = CTkButton(root, text="Retour", width=10, command=return_to_previous_frame)
    return_button.pack(pady=0, padx=0)
    rechercher_buttonJ=CTkButton(root, width=25, cursor='hand2', text='Rechercher', command=finj)
    rechercher_buttonJ.pack(pady=1,padx=1)

#-------------------------------------------------------------------------------
#--------------------------- <-- Boutons Retour <-- ----------------------------
#-------------------------------------------------------------------------------

def return_to_previous_frame():
    frame2.destroy()
    return_button.destroy()
    try:
        rechercher_buttonJ.destroy()
    except:
        pass
    ButtP()

def return_to_previous_frame_C():
    frame3.destroy()
    return_button_C.destroy()
    for i in range(len(ListeFin)):
        for j in range(len(ListeFin[i])):
            ListeFin[i].pop()
    try:
        rechercher_buttonF.destroy()
        rechercher_buttonS.destroy()
    except: pass
    BCsuite()

def return_to_previous_frame_M():
    frame3.destroy()
    return_button_M.destroy()

    BAsuite()

def return_to_previous_frame_L():
    frame3.destroy()
    return_button_L.destroy()
    rechercher_buttonL.destroy()
    for i in range(len(ListeFin)):
        for j in range(len(ListeFin[i])):
            ListeFin[i].pop()
    BLsuite()

def return_to_previous_frameFin():
    frameFin.destroy()
    return_button_Fin.destroy()
    CS.destroy()
    if Return=="musee":
        BMsuite()
    if Return=="jeu":
        BJsuite()
    if Return=="film":
        Fsuite()
    if Return=='série':
        Ssuite()
    if Return=="litérature":
        litsuite()
    if Return=="musique":
        MusicSuite()

    

#-------------------------------------------------------------------------------
#--------------------------- ! Boutons Principaux ! ----------------------------
#-------------------------------------------------------------------------------
def ButtP():
    frame.destroy()
    if frameFin!=None:
        frameFin.destroy()
        CS.destroy()
        return_button_Fin.destroy()
    global frame1
    frame1=CTkFrame(root)
    frame1.pack(expand=YES)
    cmd_start=[BCsuite,BAsuite,BLsuite,BMsuite,BJsuite]
    f=38
    i,x,y=0,0,0

    for bouton in bouton_start:
        if len(bouton)>5:
            f=38-len(bouton)
        if len(bouton)==5:
            f=38    
        if x>2:
            x=0
            y+=1

        Btn=CTkButton(frame1, width=25, cursor="hand2", text=bouton, command=cmd_start[i])
        Btn.grid(row=x, column=y, pady=10, padx=10, ipadx=f )
        i,x=i+1,x+1
        

#-------------------------------------------------------------------------------
#--------------------------- > Fonctions d'appel de donnée < -------------------
#-------------------------------------------------------------------------------
def switch_eventS(id,var):
    if switch_varS[var].get():
        IDS.append(id.get())

    else:
        IDS.remove(id.cget('onvalue'))

def switch_eventF(id,var):

    if switch_varF[var].get():
        IDF.append(id.get())

    else:
        IDF.remove(id.cget('onvalue'))

def switch_eventL(txt,var):
    if switch_varL[var].get()=='False':
        IDL.remove(txt.cget('onvalue'))

    else:
        IDL.append(txt.cget('onvalue'))


def switch_eventJ(txt,var):
    if switch_varJ[var].get()=='False':
        IDJ.remove(txt.cget('onvalue'))

    else:
        IDJ.append(txt.cget('onvalue'))

def typeMusique(a):
    global typem
    typem = a
    MusicSuite()
    
def typeLiterature(a):
    global typel
    typel=a
    litsuite()
#-------------------------------------------------------------------------------
#--------------------------- > Fonctions Des différentes fins < ----------------
#-------------------------------------------------------------------------------    
def finm(a):
    if typem=="album":
        genre="genres:"+a
    else:
        genre="genre:"+a
    info.get_Musique(genre,typem)
    global Return
    Return='musique'
    fin()

def finf():
    info.get_Film(IDF)
    global Return
    Return="film"
    fin()


def fins():
    info.get_Serie(IDS)
    global Return
    Return="série"
    fin()
    
def finl():
    a=''
    if typel!="book":
        a=a+typel+' '
    b=len(IDL)
    if typel=='manga':
        for i in range(b):
            a=IDL[i]+' '
    else:
        for i in range(b):
            a=IDL[i]+' '+a
    info.get_Livre(a)
    global Return
    Return='litérature'
    fin()

def finmu(a):
    info.get_musee(a)
    global Return
    Return='musee'
    fin()

def finj():
    info.get_Jeu(IDJ)
    global Return
    Return="jeu"
    fin()

#-------------------------------------------------------------------------------
#--------------------------- > Fonctions transformation images < ---------------
#-------------------------------------------------------------------------------
def obtenir_image_tk(url):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    img_tk = CTkImage(img,size=(250,250))
    return img_tk

    
#-------------------------------------------------------------------------------
#---------------------------- > Boutons de Départ < ----------------------------
#-------------------------------------------------------------------------------
frame=CTkFrame(root)
frame.pack(expand=YES)

S=CTkButton(frame, width=25, cursor="hand2", text="Commencer", command=ButtP)
S.grid(row=0, column=0)

root.mainloop()