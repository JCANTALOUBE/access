import requests
import json
from random import randint


class Info:
    '''Va donner des caractéristique a tout'''
    def __init__(self):
          self.nom="Il n'y a pas de noms disponible"
          self.description="Il n'y a pas de description disponible"
          self.image="Il n'y a pas d'image Disponible"
          
          
    def get_Film(self,genre=list):
        """Cette fonction permet de récuperer des infos sur n'importe quel film en fonction d'un genre définis"""
        a=[]
        txtgenre=''
        for i in range(len(genre)):
                if i!=len(genre)-1:
                    txtgenre=txtgenre+str(genre[i])+'%2C'
                else:
                    txtgenre=txtgenre+str(genre[i])
        for i in range (5):
            

            url = f"https://api.themoviedb.org/3/discover/movie?language=fr&page={i+1}&with_genres={txtgenre}"

            headers = {
                "accept": "application/json",
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjOThlMTFjZGNkM2ZlNDUyNzY3N2RlNjM2MmQwMGJiOCIsInN1YiI6IjY1ODAwNTI2MjI2YzU2MDg1OTlkZGMwNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.R4FcO5vPCsPZf8AtxTCHNF0JUzieg6QVUT-wFbv0WWQ"
            }
            response = requests.get(url, headers=headers)
            tabJSON = json.loads(response.text)
            for i in range(len(tabJSON["results"])):
                a.append(tabJSON['results'])
        try:
            b=a[randint(0,4)]
            b=b[randint(0,len(b)-1)]

            self.nom=b['title']
            self.description=b['overview']
            self.image='https://image.tmdb.org/t/p/original'+b['poster_path']
        except:
            self.nom='Echec de la récupération'
            self.description='Echec de la récupération'
            self.image='Echec de la récupération'
    
    def get_Serie(self,genre=list):
        """Cette fonction permet de récuperer des infos sur n'importe quelle serie en fonction d'un genre définis"""
        a=[]
        txtgenre=''
        for i in range(len(genre)):
                if i!=len(genre)-1:
                    txtgenre=txtgenre+str(genre[i])+'%2C'
                else:
                    txtgenre=txtgenre+str(genre[i])
        for i in range (5):
            

            url = f"https://api.themoviedb.org/3/discover/tv?language=fr&page={i+1}&with_genres={txtgenre}"

            headers = {
                "accept": "application/json",
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjOThlMTFjZGNkM2ZlNDUyNzY3N2RlNjM2MmQwMGJiOCIsInN1YiI6IjY1ODAwNTI2MjI2YzU2MDg1OTlkZGMwNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.R4FcO5vPCsPZf8AtxTCHNF0JUzieg6QVUT-wFbv0WWQ"
            }
            response = requests.get(url, headers=headers)
            tabJSON = json.loads(response.text)
            for i in range(len(tabJSON["results"])):
                a.append(tabJSON['results'])
        try:
            b=a[randint(0,4)]
            b=b[randint(0,len(b)-1)]
            self.nom=b['name']
            self.description=b['overview']
            self.image='https://image.tmdb.org/t/p/original'+b['poster_path']
        except:
           self.nom='Echec de la récupération'
           self.description='Echec de la récupération'
           self.image='Echec de la récupération'
            
    def get_Jeu(self,genre_slug=list):
        """Cette fonction permet de récuperer des infos sur n'importe quel jeu en fonction d'un genre définis"""
        api_key = "75b08ba1c8c34645be3b62e11989dbaf"
        base_url = "https://api.rawg.io/api/games"
        
        params = {
            "key": api_key,
            "genres": genre_slug
        }

        response = requests.get(base_url, params=params)
        response.raise_for_status()
        data = response.json()


        games= data.get("results", [])
        
        
        try:
            b=games[randint(0,len(games)-1)]
            self.nom=b["name"]
            self.image=b["background_image"]
        except: 
            self.nom='Echec de la récupération'
            self.image='Echec de la récupération'
            
    def get_Livre(self,genre=str):
        """Cette fonction permet de récuperer des infos sur n'importe quel livre en 
        fonction d'un genre définis et d'un type de livre
        """
        clé= Info.search_books(genre)
        reponse = requests.get(f'https://www.googleapis.com/books/v1/volumes/{clé[randint(0,len(clé)-1)]}')
        tabJSON = json.loads(reponse.text)
        try:
            self.nom=tabJSON["volumeInfo"]["title"]
        except:
            self.nom='Echec de la récupération'
        try: 
            self.description=tabJSON['volumeInfo']['description']
        except:
            self.description='Echec de la récupération'
        try:
            self.image=tabJSON['volumeInfo']['imageLinks']["thumbnail"]
        except:
            self.image='Echec de la récupération'

    def search_books(genre=str):
        """Cette fonction permet de récuperer des ids de livre en fonction d'un genre définis"""
        base_url = 'https://www.googleapis.com/books/v1/volumes'
        book_ids = set()
        start_index = 0

        while len(book_ids) < 40:
            params = {
                'q': f'subject:{genre}',
                'key': "AIzaSyDWWf_PeYhtfDVf0fXntMkwbwORJJLG_mo",
                'startIndex': start_index,
                'maxResults': 40
            }

            response = requests.get(base_url, params=params)

            if response.status_code == 200:
                data = response.json()
                books = data.get('items', [])

                if not books:
                    break  # Arrêter si aucun livre trouvé

                for book in books:
                    book_id = book.get('id', 'N/A')  # Récupérer l'ID du livre
                    book_ids.add(book_id)

                start_index +=40

            else:
                print(f"Error: {response.status_code}")
                break

        return list(book_ids)[:40]
    
    
    def get_musee(self,genre=str):
        """Cette fonction permet de récuperer des infos sur n'importe quel musee en fonction d'un genre définis"""
        f = open("DataMusee.json","rt", encoding='UTF8')
        lecteurJSON = json.loads(f.read())
        b=[]
        for objet in lecteurJSON:
            try:
                if genre in objet['fields']['themes']:
                    z=[]
                    z.append(objet['fields']['nomoff'])
                    z.append(objet['fields']['themes'])
                    z.append(objet['fields']['region'])
                    z.append(objet['fields']['ville_m'])
                    b.append(z)
            except:
                pass
        try:
            b=b[randint(0,len(b)-1)]
            self.nom=b[0]
            self.description="Ce musée contient tout ces thèmes : "+b[1]
            self.image="Il n'y a pa d'image disponible pour cette recherche"
        except:
            self.nom='Echec de la récupération'
            self.description='Echec de la récupération'
            self.image='Echec de la récupération'
    
    def Token():
        
        url = "https://accounts.spotify.com/api/token"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
        }

        data = {
            "grant_type": "client_credentials",
            "client_id": "d8cdc9e16ffe461e981e4d45340d13d0",
            "client_secret": "e62d667816db49edbd84e2937ff27c70",
        }

        response = requests.post(url, headers=headers, data=data)

        if response.status_code == 200:
            token_info = response.json()
            access_token = token_info.get("access_token")
            return access_token
        else:
            return f"Error: {response.status_code}, {response.text}"
        
    def search_spotify(query, search_type):
        url = "https://api.spotify.com/v1/search"
        headers = {
            "Authorization": f"Bearer {Info.Token()}"
        }

        params = {
            "q": query,
            "type": search_type,
            "limit": 50  # Spécifie le nombre maximal de résultats à retourner
        }

        response = requests.get(url, headers=headers, params=params)

        if response.status_code == 200:
            search_results = response.json()
            if search_type == 'track':
                return [item['id'] for item in search_results['tracks']['items']]
            elif search_type == 'artist':
                return [item['id'] for item in search_results['artists']['items']]
            elif search_type == 'album':
                return [item['id'] for item in search_results['albums']['items']]
            else:
                print(f"Type de recherche non pris en charge: {search_type}")
                return None
        else:
            print(f"Error: {response.status_code}, {response.text}")
            return None
        
    def get_Musique(self,genre=str,type=str):
        """Cette fonction permet de récuperer des infos sur n'importe qu'elle musique en fonction d'un genre définis"""
        track_id=Info.search_spotify(genre,type)
        type=type+"s"

        
        url = f"https://api.spotify.com/v1/{type}/{track_id[randint(0,len(track_id)-1)]}"
        headers = {
            "Authorization": f"Bearer {Info.Token()}"
        }

        response = requests.get(url, headers=headers)
        track_info = json.loads(response.text)
        if type=='artists':
            try :
                if track_info:
                    self.nom=track_info['name']
                    self.description="Cet artiste a "+str(track_info['followers']['total'])+" abonnées."
                    self.image=track_info['images'][0]['url']
            except:
                    self.nom='Echec de la récupération'
                    self.image='Echec de la récupération'
        if type=='albums':
            try:
                if track_info:
                    self.description="Il y a "+str(track_info["total_tracks"])+" musique dans cette album."
                    self.nom=track_info["name"]
                    
                    self.image=track_info["images"][0]["url"]


            except:
                self.nom='Echec de la récupération'
                self.image='Echec de la récupération'
        if type=="tracks":
            try:
                if track_info:
                    self.nom=track_info['name']
                    self.image=track_info["album"]["images"][0]["url"]
            except:
                self.nom='Echec de la récupération'
                self.image='Echec de la récupération'
                





    



